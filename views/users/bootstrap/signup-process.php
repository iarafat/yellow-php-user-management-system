<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users;


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $obj->prepare($_POST);
    $obj->signup();
    if (!empty($_SESSION['Login_data']['is_admin']) == 0) {
        if (!empty($_POST['user_name']) && !empty($_POST['password']) && !empty($_POST['email'])) {
            $to = $_POST['email'];
            $subject = 'Signup | Verification';
            $message = '
            Thanks for signing up!<br><br>
            Activated your account by pressing the url below.<br><br>
            ------------------------<br>
            Username: ' . $_POST['user_name'] . '<br><br>
            Password: ' . $_POST['password'] . '<br>
            ------------------------<br><br>
            Please click this link to activate your account:<br><br>
            http://themeyellow.com/yellow/views/users/materialize/verify.php?vid=' . $_SESSION['vid'] . '<br><br>
            Thank you.
            ';

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:ThemeYellow<info@themeyellow.com>' . "\r\n";
            @mail($to, $subject, $message, $headers);
        }
    }
} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}

?>