<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------
$config = array(
    "base_url" => "http://localhost/signup&login/views/users/bootstrap/hybridauth/index.php",
    "providers" => array(

        "Google" => array(
            "enabled" => true,
            "keys" => array("id" => "1032209224024-6j6l81mdmims1cih30rphqjvomo7bdur.apps.googleusercontent.com", "secret" => "y-o3Kv8ZzHOSYzybu_dAII3D"),
        ),

        "Facebook" => array(
            "enabled" => true,
            "keys" => array("id" => "187754778322658", "secret" => "a9a0bf8bc98913d02dc46fc3837a6395"),
            "scope" => "email, user_photos,user_location, user_about_me, user_birthday, user_hometown",
        ),

        "Twitter" => array(
            "enabled" => true,
            "includeEmail" => true,
            "keys" => array("key" => "nhpDV3vzD1ptyFts2Uro2bkdh", "secret" => "lg7CXVFlcfjDMPcGW9GisJQsWwvrskxEB7SUQ24XRXaYBdGNLS"),
        ),
    ),
    // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
    "debug_mode" => false,
    "debug_file" => "",
);
