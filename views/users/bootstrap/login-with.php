<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users;

include_once('config.php');
include_once('hybridauth/Hybrid/Auth.php');
if (isset($_GET['provider'])) {
    $provider = $_GET['provider'];
    try {
        $hybridauth = new Hybrid_Auth($config);
        $authProvider = $hybridauth->authenticate($provider);
        $user_profile = $authProvider->getUserProfile();


        $_POST['identifier'] = $user_profile->identifier;
        $_POST['displayname'] = $user_profile->displayName;
        $_POST['photourl'] = $user_profile->photoURL;
        $_POST['email'] = $user_profile->email;
        $_POST['provider'] = $_GET['provider'];

        $obj->prepare($_POST);
        $obj->socialLogin();

    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }

}
?>