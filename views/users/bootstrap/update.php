<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (!empty($_FILES['images']['name'])) {
        $photo_name = time() . $_FILES['images']['name'];
        $photo_type = $_FILES['images']['type'];
        $photo_tmp_name = $_FILES['images']['tmp_name'];
        $photo_size = $_FILES['images']['size'];
        $photo_extension = strtolower(end(explode('.', $photo_name)));
        //print_r($photo_extension);
        $required_format = array('jpg', 'jpeg', 'png', 'gif');
        if (in_array($photo_extension, $required_format) === false) {
            $_SESSION['photo_error'] = "Invalid file format";
        } elseif ($photo_size > 2000000) {
            $_SESSION['photo_error'] = "Image size too large";
        } else {
            move_uploaded_file($photo_tmp_name, 'photos/' . $photo_name);
            $_POST['images'] = $photo_name;
        }
    }

    $obj->prepare($_POST);
    $obj->proUpdate();

} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}
?>
