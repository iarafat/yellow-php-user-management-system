<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">

    <title>Yellow - PHP User Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <link href="build/css/form.style.css" rel="stylesheet">
</head>

<body class="login">
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-1">
            <div class="form-log-info">
                <h3><strong>Yellow - User Management System</strong></h3><br>
                <h4>Login Details</h4>
                <div class="clearfix"></div>
                <div class="separator">
                    <h4>Admin</h4>
                    <strong>User Name: </strong><span>arafat</span><br>
                    <strong>Password: </strong><span>arafat123</span><br>
                    <h4>Manager</h4>
                    <strong>User Name: </strong><span>iyasinarafat</span><br>
                    <strong>Password: </strong><span>iyasinaraf123</span><br>
                    <h4>User</h4>
                    <strong>User Name: </strong><span>iyasin</span><br>
                    <strong>Password: </strong><span>iyasin123</span>
                </div>
            </div>

        </div>
        <div class="col-md-5 col-md-offset-1">
            <div class="form_wrapperlog">
                <div class="animate form login_form">
                    <section class="form_content">
                        <form action="login-process.php" method="post" id="loginForm">
                            <h1>Login Form</h1>
                            <strong class="suc"><?php $obj->ValidationMessage("storeSuccess"); ?></strong>
                            <strong class="suc"><?php $obj->ValidationMessage("confirm"); ?></strong>
                            <strong class="suc"><?php $obj->ValidationMessage("Pass_U"); ?></strong>
                            <strong class="suc"><?php $obj->ValidationMessage("Logout_M"); ?></strong>
                            <strong class="deng"><?php $obj->ValidationMessage("E_active"); ?></strong>
                            <strong class="deng"><?php $obj->ValidationMessage("U_P"); ?></strong>

                            <div>
                                <input type="text" name="log_user_name" class="form-control" placeholder="Username"/>
                            </div>
                            <div>
                                <input type="password" name="log_password" class="form-control" placeholder="Password"/>
                            </div>
                            <div>
                                <div class="g-recaptcha" data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha"
                                       id="hiddenRecaptcha">
                            </div>
                            <div>
                                <button class="btn btn-default submit" id="log">Log in</button>
                                <a class="reset_pass" href="forgot.php?id=reset">Lost your password?</a>
                                <a class="reset_uname" href="forgot.php?id=remind">Lost your Username?</a>
                            </div>
                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">New here?
                                    <a href="signup.php" class="to_register"> Create Account </a>
                                    <a class="resent" href="forgot.php?id=resent">Resent Activation?</a>
                                </p>
                                <br/>
                                <div class="clearfix"></div>
                                <div class="separator social-log">
                                    <h4 class="change_link">Social Login</h4>
                                    <a href="login-with.php?provider=Facebook"><i class="fa fa-facebook"
                                                                                  aria-hidden="true"></i></a>
                                    <a href="login-with.php?provider=Twitter"><i class="fa fa-twitter"
                                                                                 aria-hidden="true"></i></a>
                                    <a href="login-with.php?provider=Google"><i class="fa fa-google-plus"
                                                                                aria-hidden="true"></i></a>
                                </div>

                                <br/>
                                <br/>
                                <div>
                                    <h1><i class="fa fa-shield"></i> Yellow</h1>
                                    <p>©2016 All Rights Reserved.</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="vendors/nprogress/nprogress.js"></script>
<!-- recaptcha api -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- jquery.validate -->
<script src="vendors/jqueryvalidation/jquery.validate.min.js"></script>
<script src="vendors/jqueryvalidation/additional-methods.min.js"></script>
<!-- custom validation script -->
<script>
    $("#loginForm").validate({
        ignore: ".ignore",
        rules: {
            log_user_name: {
                required: true
            },
            log_password: {
                required: true
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            log_user_name: {
                required: "Please enter user name"
            },
            log_password: {
                required: "Please enter password"
            },
            hiddenRecaptcha: "You must complete the anti spam verification"

        }
    });
</script>
<!-- Custom Theme Scripts -->
<script src="build/js/custom.js"></script>
</body>
</html>
