<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
$obj->prepare($_GET);
$obj->loginCheck();
$login_data = $obj->LoginShow();
$profile_data = $obj->show();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">
    <title>Yellow - PHP User Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- mCustomScrollbar.css -->
    <link href="vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <link href="build/css/form.style.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="" class="site_title"><i class="fa fa-shield"></i> <span>Yellow</span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <?php
                        if(!empty($login_data['photourl'])) {
                            echo "<img class='img-circle profile_img' src='" . $login_data['photourl'] . "' alt='image'>";
                        } elseif ($login_data['images'] == "") {
                            echo "<img class='img-circle profile_img' src='photos/defult-pic.png' alt='Default image'>";
                        } else {
                            echo "<img class='img-circle profile_img' src='photos/" . $login_data['images'] . "' alt='image'>";
                        }
                        ?>
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2><?php echo $login_data['user_name']; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="index.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-home"></i> Dashboard</a></li>
                            <?php if (!empty($_SESSION['Login_data']['is_admin']) == 1){ ?>
                                <li><a href="add-new-user.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-users"></i> Add New User</a></li>
                            <?php }?>
                            <li><a href="show.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-user"></i> Profile</a></li>

                            <?php if (empty($_SESSION['Login_data']['provider'])){ ?>
                                <li><a href="edit.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-edit"></i> Profile Update</a></li>

                                <li><a href="account-update.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-check-square-o"></i> Account Update</a></li>
                                <?php if (!empty($_SESSION['Login_data']['is_admin']) == 1 OR !empty($_SESSION['Login_data']['is_manager']) == 1){ ?>
                                    <li><a href="trash-items.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                                class="fa fa-trash"></i> Trash Items</a></li>
                                <?php } } ?>

                            <li><a href="logout.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- /menu_fixed -->
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <?php
                                if(!empty($login_data['photourl'])) {
                                    echo "<img src='" . $login_data['photourl'] . "' alt='image'>";
                                } elseif ($login_data['images'] == "") {
                                    echo "<img src='photos/defult-pic.png' alt='Default image'> ";
                                } else {
                                    echo "<img src='photos/" . $login_data['images'] . "' alt='image'> ";
                                }
                                echo $login_data['user_name'];
                                ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="show.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>">
                                        Profile</a></li>
                                <li><a href="logout.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left">
                    <div class="col-md-3 col-sm-3 col-xs-12 ">
                        <h3>Profile View</h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-6 col-md-offset-3 col-sm-6 col-xs-12 mart30">
                <div class="x_panel">
                    <div class="x_content profile">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php if (empty($_SESSION['Login_data']['provider'])) { ?>
                                    <div class="pro-data">
                                        <label>Name: </label> <?php echo $profile_data['full_name'] ?>
                                    </div>
                                    <div class="pro-data">
                                        <label>Gender: </label> <?php echo $profile_data['gender'] ?>
                                    </div>
                                    <div class="pro-data">
                                        <label>Phone: </label> <?php echo $profile_data['phone'] ?>
                                    </div>
                                    <div class="pro-data">
                                        <label>Address: </label> <?php echo $profile_data['address'] ?>
                                    </div>
                                <?php } else { ?>
                                    <div class="pro-data">
                                        <label>Name: </label> <?php echo $profile_data['user_name'] ?>
                                    </div>
                                <?php } ?>
                                <div class="pro-data">
                                    <label>Active: </label>
                                    <?php if ($profile_data['is_active'] == 1) {
                                        echo "<i class=\"fa fa-check-square activity\" aria-hidden=\"true\"></i>";
                                    } else {
                                        echo "<i class=\"fa fa-times-circle activity\" aria-hidden=\"true\"></i>";
                                    } ?>
                                </div>
                                <?php if (!empty($_SESSION['Login_data']['is_admin']) == 1 OR !empty
                                    ($_SESSION['Login_data']['is_manager']) == 1
                                ) { ?>
                                    <div class="pro-data">
                                        <label>Admin: </label>
                                        <?php if ($profile_data['is_admin'] == 1) {
                                            echo "<i class=\"fa fa-check-square activity\" aria-hidden=\"true\"></i>";
                                        } else {
                                            echo "<i class=\"fa fa-times-circle activity\" aria-hidden=\"true\"></i>";
                                        } ?>
                                    </div>
                                    <div class="pro-data">
                                        <label>Manager: </label>
                                        <?php if ($profile_data['is_manager'] == 1) {
                                            echo "<i class=\"fa fa-check-square activity\" aria-hidden=\"true\"></i>";
                                        } else {
                                            echo "<i class=\"fa fa-times-circle activity\" aria-hidden=\"true\"></i>";
                                        } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 profile">
                                <label>Profile Picture: </label>
                                <?php
                                if (!empty($profile_data['photourl'])) {
                                    echo "<img  class='img-b' width='100' height='100' src='" . $profile_data['photourl'] . "' alt='image'>";
                                } elseif ($profile_data['images'] == "") {
                                    echo "<img class='img-b' width='100' height='100' src='photos/defult-pic.png' alt='Default image'>";
                                } else {
                                    echo "<img class='img-b' width='100' height='100' src='photos/" . $profile_data['images'] . "' alt='Updated image'>";
                                }
                                ?>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="pro-data">
                                    <label>Created: </label> <?php echo $profile_data['created'] ?>
                                </div>
                                <?php if (empty($_SESSION['Login_data']['provider'])){ ?>
                                <div class="pro-data">
                                    <label>Modified: </label> <?php echo $profile_data['modified'] ?>
                                </div>
                                <div class="pro-data">
                                    <label>Deleted: </label> <?php echo $profile_data['deleted'] ?>
                                </div>
                                <div class="pro-data">
                                    <a class="btn btn-success" href="edit.php?id=<?php echo $profile_data['unique_id']
                                    ?>">Edit</a>
                                    <?php if ($profile_data['is_admin'] == 0) { ?>
                                        <a class="btn btn-default" href="trash.php?id=<?php echo
                                        $profile_data['unique_id'] ?>"
                                           onclick="return confirm('Are you sure?');">Delete</a>
                                    <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">Yellow - PHP User Management System</div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="vendors/iCheck/icheck.min.js"></script>
<!-- mCustomScrollbar -->
<script src="vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- recaptcha api -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- jquery.validate -->
<script src="vendors/jqueryvalidation/jquery.validate.min.js"></script>
<script src="vendors/jqueryvalidation/additional-methods.min.js"></script>

<!-- custom validation script -->
<script>

    $("#updateForm").validate({
        ignore: ".ignore",
        rules: {
            full_name: "required",
            phone: "required",
            address: "required",

            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            full_name: "Please enter your full name",
            hiddenRecaptcha: "You must complete the anti spam verification"
        }
    });

</script>
<!-- Custom Theme Scripts -->
<script src="build/js/custom.js"></script>
</body>
</html>
