<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
$obj->loginCheck();
$login_data = $obj->LoginShow();
$profile_data = $obj->show();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">
    <title>Yellow - PHP User Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- mCustomScrollbar.css -->
    <link href="vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <link href="build/css/form.style.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="" class="site_title"><i class="fa fa-shield"></i> <span>Yellow</span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <?php
                        if(!empty($login_data['photourl'])) {
                            echo "<img class='img-circle profile_img' src='" . $login_data['photourl'] . "' alt='image'>";
                        } elseif ($login_data['images'] == "") {
                            echo "<img class='img-circle profile_img' src='photos/defult-pic.png' alt='Default image'>";
                        } else {
                            echo "<img class='img-circle profile_img' src='photos/" . $login_data['images'] . "' alt='image'>";
                        }
                        ?>
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2><?php echo $login_data['user_name']; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="index.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-home"></i> Dashboard</a></li>
                            <?php if (!empty($_SESSION['Login_data']['is_admin']) == 1){ ?>
                                <li><a href="add-new-user.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-users"></i> Add New User</a></li>
                            <?php }?>
                            <li><a href="show.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-user"></i> Profile</a></li>

                            <?php if (empty($_SESSION['Login_data']['provider'])){ ?>
                                <li><a href="edit.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-edit"></i> Profile Update</a></li>

                                <li><a href="account-update.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-check-square-o"></i> Account Update</a></li>
                                <?php if (!empty($_SESSION['Login_data']['is_admin']) == 1 OR !empty($_SESSION['Login_data']['is_manager']) == 1){ ?>
                                    <li><a href="trash-items.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                                class="fa fa-trash"></i> Trash Items</a></li>
                                <?php } } ?>

                            <li><a href="logout.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                        class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- /menu_fixed -->
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <?php
                                if(!empty($login_data['photourl'])) {
                                    echo "<img src='" . $login_data['photourl'] . "' alt='image'>";
                                } elseif ($login_data['images'] == "") {
                                    echo "<img src='photos/defult-pic.png' alt='Default image'> ";
                                } else {
                                    echo "<img src='photos/" . $login_data['images'] . "' alt='image'> ";
                                }
                                echo $login_data['user_name'];
                                ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="show.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>">
                                        Profile</a></li>
                                <li><a href="logout.php?id=<?php echo $_SESSION['Login_data']['unique_id'] ?>"><i
                                            class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left">
                    <div class="col-md-3 col-sm-3 col-xs-12 ">
                        <h3>Add New User</h3>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <!-- Messages -->
                    <h3 class="suc"><?php $obj->ValidationMessage("storeSuccess"); ?></h3>
                </div>
            </div>
            <?php
            if (!empty($_SESSION['Login_data']['is_admin']) == 1 OR !empty($_SESSION['Login_data']['is_manager'])
                == 1
            ) {

                ?>
                <div class="clearfix"></div>

                <div class="col-md-6 col-md-offset-3 col-sm-6 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <!-- start form for validation -->
                            <form action="signup-process.php" method="post" id="signupForm">
                                <label for="user_name">User Name</label>
                                <input type="text" name="user_name" class="form-control" id="user_name">
                                <br>
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password">
                                <br>
                                <label for="re_password">Re-Password</label>
                                <input type="password" name="re_password" class="form-control" id="re_password">
                                <br>
                                <label for="email">Email</label>
                                <input type="email" id="email" class="form-control" name="email">
                                <br>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="flat" id="active" name="is_active" value="1">
                                        Active
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="flat" id="manager" name="is_manager" value="1">
                                        Manager
                                    </label>
                                </div>
                                <br>
                                <div>
                                    <div class="g-recaptcha"
                                         data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                                    <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha"
                                           id="hiddenRecaptcha">
                                </div>
                                <br>
                                <div>
                                    <button class="btn btn-success submit">Add User</button>
                                </div>
                            </form>
                            <!-- end form for validations -->

                        </div>
                    </div>
                </div>
                <?php
            } else {
                $_SESSION['Errors_R'] = "User not found :(";
                header("location:errors.php");
            }
            ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">Yellow - PHP User Management System</div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="vendors/iCheck/icheck.min.js"></script>
<!-- mCustomScrollbar -->
<script src="vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- recaptcha api -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- jquery.validate -->
<script src="vendors/jqueryvalidation/jquery.validate.min.js"></script>
<script src="vendors/jqueryvalidation/additional-methods.min.js"></script>

<!-- custom validation script -->
<script>
    /*User Name*/
    $.validator.addMethod("user_name_valid", function (value) {
        if (/^[a-zA-Z0-9_\.]+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "The username can only consist of alphabetical, number, dot and underscore");

    /*Password*/
    $.validator.addMethod("pwcheck", function (value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value)
    });

    /*Email*/
    $.validator.addMethod("validemail", function (value) {
        if (value == '')
            return true;
        var temp1;
        temp1 = true;
        var ind = value.indexOf('@');
        var str2 = value.substr(ind + 1);
        var str3 = str2.substr(0, str2.indexOf('.'));
        if (str3.lastIndexOf('-') == (str3.length - 1) || (str3.indexOf('-') != str3.lastIndexOf('-')))
            return false;
        var str1 = value.substr(0, ind);
        if ((str1.lastIndexOf('_') == (str1.length - 1)) || (str1.lastIndexOf('.') == (str1.length - 1)) || (str1.lastIndexOf('-') == (str1.length - 1)))
            return false;
        str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$/;
        temp1 = str.test(value);
        return temp1;
    }, "Please enter valid email");

    $("#signupForm").validate({
        ignore: ".ignore",
        rules: {
            user_name: {
                user_name_valid: true,
                required: true,
                minlength: 6,
                maxlength: 12,
                remote: "validations.php?data=name"
            },
            password: {
                pwcheck: true,
                required: true,
                minlength: 6,
                maxlength: 12
            },
            re_password: {
                required: true,
                equalTo: "#password"
            },
            email: {
                validemail: true,
                required: true,
                email: true,
                remote: "validations.php?data=email"
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            hiddenRecaptcha: "You must complete the anti spam verification",
            user_name: {
                required: "Please enter a username",
                minlength: "Please enter 6-12 characters",
                maxlength: "Please enter 6-12 characters",
                remote: $.validator.format("'{0}' is already exists")
            },
            password: {
                pwcheck: "Allowed Characters: 'A-Z a-z 0-9 @ * _ - . !' for password",
                required: "Please provide a password",
                minlength: "Please enter 6-12 characters",
                maxlength: "Please enter 6-12 characters"
            },
            re_password: {
                required: "Please provide a re-password",
                equalTo: "Please enter the same password as above"
            },
            email: {
                required: "Please provide a email address",
                email: "Please enter a valid email address",
                remote: $.validator.format("'{0}' is already exists")
            }
        }
    });
</script>
<!-- Custom Theme Scripts -->
<script src="build/js/custom.js"></script>
</body>
</html>
