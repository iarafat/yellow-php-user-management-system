<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $obj->prepare($_POST);
    $obj->login();
} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}

?>