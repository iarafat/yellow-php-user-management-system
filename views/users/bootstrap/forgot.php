<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">

    <title>Yellow - PHP User Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <link href="build/css/form.style.css" rel="stylesheet">
</head>

<body class="login">
<div class="form_wrapper">
    <div class="animate form login_form">
        <section class="form_content">
                    <?php if (isset($_GET)&& !empty($_GET)) { ?>
                    <form action="forgot-process.php" method="post" id="forgotForm">
                        <strong class="suc"> <?php $obj->ValidationMessage("foM"); ?></strong>
                        <strong class="deng"><?php $obj->ValidationMessage("Not"); ?></strong>
                        <strong class="deng"><?php $obj->ValidationMessage("Nots"); ?></strong>

                        <!--password-->
                        <?php if ($_GET['id'] == "reset") { ?>
                            <p>Please enter the email address for your account. A verification code will be sent to you.
                                Once you have received the verification code, you will be able to choose a new password
                                for your account.</p>
                            <div>
                                <input type="email" name="email" class="form-control" placeholder="Email"/>
                            </div>
                            <div>
                                <div class="g-recaptcha" data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                            </div>
                        <?php } ?>

                        <?php if ($_GET['id'] == "pconfirm") { ?>
                            <p>Please enter user name in the field below to prove that you are the owner of this
                                account.</p>

                            <div>
                                <input type="text" name="user_name" class="form-control" placeholder="Username"/>
                            </div>
                        <?php } ?>

                        <?php if ($_GET['id'] == "pcomplete") {
                            if (isset($_SESSION['forgotP']) && !empty($_SESSION['forgotP'])) {
                                ?>
                                <p>To complete the password reset process, please enter a new password.</p>
                                <div>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
                                </div>
                                <div>
                                    <input type="password" name="re_password" class="form-control" placeholder="Repassword"/>
                                </div>
                                <?php
                            } else {
                                $_SESSION['Errors_R'] = "You could not access this page.";
                                header("location:errors.php");
                            }
                        } ?>
                        <!--/password-->

                        <!--user name-->
                        <?php if ($_GET['id'] == "remind") { ?>
                            <p>Please enter the email address associated with your User account. Your username will be emailed to the email address on file.</p>
                            <div>
                                <input type="email" name="email" class="form-control" placeholder="Email"/>
                            </div>
                            <div>
                                <div class="g-recaptcha" data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                            </div>
                        <?php } ?>
                        <!--/user name-->

                        <!--resent-->
                        <?php if ($_GET['id'] == "resent") { ?>
                            <p>Resend Activation Email</p>

                            <div>
                                <input type="email" name="email" class="form-control" placeholder="Email"/>
                            </div>
                            <div>
                                <div class="g-recaptcha" data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                            </div>
                        <?php } ?>
                        <!--/resent-->

                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <div>
                            <button class="btn btn-default submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>

                        <div class="separator">
                            <a href="signup.php">SignUp Page</a><a href="login.php"> Login Page</a>
                            <br />
                            <div>
                                <h2><i class="fa fa-shield"></i> Yellow</h2>
                                <p>©2016 All Rights Reserved.</p>
                            </div>
                        </div>
                    </form>
                    <?php } else {
                        $_SESSION['Errors_R'] = "You could not access this page.";
                        header("location:errors.php");
                    } ?>
        </section>
    </div>
</div>

<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="vendors/iCheck/icheck.min.js"></script>
<!-- recaptcha api -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- jquery.validate -->
<script src="vendors/jqueryvalidation/jquery.validate.min.js"></script>
<script src="vendors/jqueryvalidation/additional-methods.min.js"></script>

<!-- custom validation script -->
<script>

    /*Password*/
    $.validator.addMethod("pwcheck", function (value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value)
    });

    /*Email*/
    $.validator.addMethod("validemail", function (value) {
        if (value == '')
            return true;
        var temp1;
        temp1 = true;
        var ind = value.indexOf('@');
        var str2 = value.substr(ind + 1);
        var str3 = str2.substr(0, str2.indexOf('.'));
        if (str3.lastIndexOf('-') == (str3.length - 1) || (str3.indexOf('-') != str3.lastIndexOf('-')))
            return false;
        var str1 = value.substr(0, ind);
        if ((str1.lastIndexOf('_') == (str1.length - 1)) || (str1.lastIndexOf('.') == (str1.length - 1)) || (str1.lastIndexOf('-') == (str1.length - 1)))
            return false;
        str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$/;
        temp1 = str.test(value);
        return temp1;
    }, "Please enter valid email");

    $("#forgotForm").validate({
        ignore: ".ignore",
        rules: {
            user_name: "required",
            password: {
                pwcheck: true,
                required: true,
                minlength: 6,
                maxlength: 12
            },
            re_password: {
                required: true,
                equalTo: "#password"
            },
            email: {
                validemail: true,
                required: true,
                email: true
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            email: {
                required: "Please enter email address"
            },
            password: {
                pwcheck: "Allowed Characters: 'A-Z a-z 0-9 @ * _ - . !' for password",
                required: "Please provide a password",
                minlength: "Please enter 6-12 characters",
                maxlength: "Please enter 6-12 characters"
            },
            re_password: {
                required: "Please provide a re-password",
                equalTo: "Please enter the same password as above"
            },
            hiddenRecaptcha: "You must complete the anti spam verification"
        }
    });
</script>
<!-- Custom Theme Scripts -->
<script src="build/js/custom.js"></script>
</body>
</html>
