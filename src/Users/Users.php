<?php

namespace App\Users;

use PDO;

class Users
{
    public $id = '';
    public $uid = '';
    public $vid = '';
    public $user_name = '';
    public $current_password = '';
    public $password = '';
    public $re_password = '';
    public $email = '';
    public $data = '';
    public $errors = '';

    public $log_user_name = '';
    public $log_password = '';

    public $con = '';

    // profile update
    public $full_name = '';
    public $gender = '';
    public $phone = '';
    public $address = '';
    public $images = '';

    // admin control
    public $is_active = '';
    public $is_manager = '';

    // Social Login
    public $identifier = '';
    public $displayname = '';
    public $photourl = '';
    public $provider = '';

    public function __construct()
    {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
//        $this->con = new PDO('mysql:host=localhost;dbname=users', "root", "");
        $this->con = new PDO('mysql:host=localhost;dbname=themeyellow_users', "themeyellow_WNbdTy", "1u!c1wp(CXAI");
    }

    public function prepare($data = '')
    {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['vid'])) {
            $this->vid = $data['vid'];
        }

        if (!empty($data['user_name'])) {
            $this->user_name = $data['user_name'];
        }

        if (!empty($data['current_password'])) {
            $this->current_password = $data['current_password'];
            $this->errors = true;
        }

        if (!empty($data['password'])) {
            $this->password = $data['password'];
            $this->errors = true;
        }
        if (!empty($data['re_password'])) {
            $this->re_password = $data['re_password'];
            $this->errors = true;
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
            $this->errors = true;
        }

        if (!empty($data['log_user_name'])) {
            $this->log_user_name = $data['log_user_name'];
        }

        if (!empty($data['log_password'])) {
            $this->log_password = $data['log_password'];
        }
        //profile update
        if (!empty($data['full_name'])) {
            $this->full_name = $data['full_name'];
        }
        if (!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (!empty($data['phone'])) {
            $this->phone = $data['phone'];
        }
        if (!empty($data['address'])) {
            $this->address = $data['address'];
        }
        if (!empty($data['images'])) {
            $this->images = $data['images'];
        }
        // admin control
        if (!empty($data['is_active'])) {
            $this->is_active = $data['is_active'];
        } else {
            $this->is_active = 0;
        }
        if (!empty($data['is_manager'])) {
            $this->is_manager = $data['is_manager'];
        } else {
            $this->is_manager = 0;
        }
        // Social Login
        if (!empty($data['identifier'])) {
            $this->identifier = $data['identifier'];
        }
        if (!empty($data['displayname'])) {
            $this->displayname = $data['displayname'];
        }
        if (!empty($data['photourl'])) {
            $this->photourl = $data['photourl'];
        }
        if (!empty($data['provider'])) {
            $this->provider = $data['provider'];
        }
    }

    public function ValidationMessage($validM = "")
    {
        if (isset($_SESSION["$validM"]) && !empty($_SESSION["$validM"])) {
            echo $_SESSION["$validM"];
            unset($_SESSION["$validM"]);
        }
    }

    public function signup()
    {
        if (!empty($this->errors) == true) {
            try {

                $this->uid = uniqid();
                $this->vid = uniqid();
                $_SESSION['vid'] = $this->vid;
                $query = "INSERT INTO signup (`id`, `unique_id`, `verification_id`, `user_name`, `password`, `email`, `is_active`,
 `is_admin`, `is_manager`, `is_delete`, `created`) VALUES (:id, :unid, :vid, :user_name,
 :password, :email, :is_active, :is_admin, :is_manager, :is_delete, :created)";
                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':unid' => $this->uid,
                    ':vid' => $this->vid,
                    ':user_name' => $this->user_name,
                    ':password' => $this->password,
                    ':email' => $this->email,
                    ':is_active' => $this->is_active,
                    ':is_admin' => 0,
                    ':is_manager' => $this->is_manager,
                    ':is_delete' => 0,
                    ':created' => date("Y-m-d h:i:s"),
                ));
                if (!empty($_SESSION['Login_data']['is_admin']) == 1) {
                    $_SESSION['storeSuccess'] = 'Successfully added user';
                    header("location:add-new-user.php?id=" . $_SESSION['Login_data']['unique_id']);
                } else {
                    $_SESSION['storeSuccess'] = 'Successfully SignUp. Check your Inbox or Spambox for verification email.';
                    header("location:login.php");
                }
                $last_id = $this->con->lastInsertId();
                $qr = "INSERT INTO profiles(id, users_id)
    VALUES(:id, :uid)";
                $statement = $this->con->prepare($qr);
                $statement->execute(array(
                    ':id' => null,
                    ':uid' => $last_id,
                ));
            } catch (Exception $exc) {
                echo 'Error: ' . $exc->getMessage();
            }
        } else {
            header("location:signup.php");
        }
    }

    public function verify()
    {
        try {
            $qr = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE verification_id=" . "'" . $this->vid . "'";
            $query = $this->con->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);

            if ($row['is_active'] == 1) {
                $_SESSION['confirm'] = "Email Already Verified.";
                header("location:login.php");
            } else {
                try {

                    $qr = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET is_active = 1 WHERE verification_id =" . "'" . $this->vid . "'";
                    $query = $this->con->prepare($qr);
                    $query->execute();
                    $_SESSION['confirm'] = "Registration Process Completed. Now you could Login";
                    header("location:login.php");
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login()
    {
        try {
            $query = "SELECT * FROM signup WHERE user_name = '$this->log_user_name'AND password='$this->log_password'";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if (isset($row) && !empty($row)) {
                if ($row['is_delete'] == 0) {
                    if ($row['is_active'] == 1) {
                        $_SESSION['Login_data'] = $row;
                        $_SESSION['Login'] = "Successfully Login";
                        header("location:index.php?id=" . $_SESSION['Login_data']['unique_id']);
                    } else {
                        $_SESSION['E_active'] = "Your account not verified yet. Check your email and verify";
                        header("location:login.php");
                    }
                } else {
                    $_SESSION['Is_D'] = "Your account was Suspended, Now you can SignUp with new email.";
                    header("location:signup.php");
                }

            } else {
                if ($row['user_name'] !== $this->log_user_name && $row['password'] !== $this->log_password) {
                    $_SESSION['U_P'] = "UserName & Password dose not matched.";
                    header("location:login.php");
                }
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function socialLogin()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE identifier=" . "'" . $this->identifier . "'";
            $query = $this->con->prepare($qur);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);

            if ($row['identifier'] == $this->identifier) {
                $_SESSION['Login_data'] = $row;
                $_SESSION['Login'] = "Successfully Login";
                header("location:index.php?id=" . $_SESSION['Login_data']['unique_id']);
            } else {
                try {
                    $this->uid = uniqid();
                    $query = "INSERT INTO signup (`id`,`unique_id`,`identifier`,`photourl`,`provider`,`user_name`,`email`,`is_active`,`created`) VALUES (:id,:unid,:identifier,:photourl,:provider,:user_name,:email,:is_active,:created)";
                    $stmt = $this->con->prepare($query);
                    $stmt->execute(array(
                        ':id' => null,
                        ':unid' => $this->uid,
                        ':identifier' => $this->identifier,
                        ':photourl' => $this->photourl,
                        ':provider' => $this->provider,
                        ':user_name' => $this->displayname,
                        ':email' => $this->email,
                        ':is_active' => 1,
                        ':created' => date("Y-m-d h:i:s"),
                    ));
                    $last_id = $this->con->lastInsertId();
                    $qr = "INSERT INTO profiles(id, users_id)
    VALUES(:id, :uid)";
                    $statement = $this->con->prepare($qr);
                    $statement->execute(array(
                        ':id' => null,
                        ':uid' => $last_id,
                    ));
                } catch (Exception $exc) {
                    echo 'Error: ' . $exc->getMessage();
                }
            }
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function loginCheck()
    {
        if (empty($_SESSION['Login_data']) && !isset($_SESSION['Login_data'])) {
            $_SESSION['Errors_R'] = "User not found :(";
            header("location:errors.php");
        }
    }

    public function logout()
    {
        try {
            $_SESSION['Logout_M'] = "Successfully logout";
            unset($_SESSION['Login_data']);
            header("location:login.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE is_delete = 0";
            $query = $this->con->prepare($qur);
            $query->execute();

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function show()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE unique_id=" . "'" . $this->id . "'";
            $query = $this->con->prepare($qur);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['acc_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function LoginShow()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE unique_id=" . "'" . $_SESSION['Login_data']['unique_id'] . "'";
            $query = $this->con->prepare($qur);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function countUsers()
    {
        try {
            $query = "SELECT COUNT(*) AS id FROM signup";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function countTrash()
    {
        try {
            $query = "SELECT COUNT(*) AS is_delete FROM signup WHERE is_delete= 1";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function countActive()
    {
        try {
            $query = "SELECT COUNT(*) AS is_active FROM signup WHERE is_active= 1";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function countAdmin()
    {
        try {
            $query = "SELECT COUNT(*) AS is_admin FROM signup WHERE is_admin= 1";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function countManager()
    {
        try {
            $query = "SELECT COUNT(*) AS is_manager FROM signup WHERE is_manager= 1";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $exc) {
            echo 'Error: ' . $exc->getMessage();
        }
    }

    public function proUpdate()
    {
        try {
            if (!empty($this->images)) {
                $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET full_name=:full_name,gender=:gender,phone=:phone,
address=:address,images=:images,modified=:modified WHERE unique_id=" . "'" . $this->id . "'";
                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ":full_name" => $this->full_name,
                    ":gender" => $this->gender,
                    ":phone" => $this->phone,
                    ":address" => $this->address,
                    ":images" => $this->images,
                    ":modified" => date("Y-m-d h:i:s"),
                ));
                $_SESSION['Pro_U'] = "Profile Updated";
                header("location:edit.php?id=$this->id");
            } else {
                $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET full_name=:full_name,gender=:gender,phone=:phone,
address=:address,modified=:modified WHERE unique_id=" . "'" . $this->id . "'";
                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ":full_name" => $this->full_name,
                    ":gender" => $this->gender,
                    ":phone" => $this->phone,
                    ":address" => $this->address,
                    ":modified" => date("Y-m-d h:i:s"),
                ));
                $_SESSION['Pro_U'] = "Profile Updated";
                header("location:edit.php?id=$this->id");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function accUpdate()
    {
        if (!empty($this->current_password)) {
            if ($_SESSION['acc_data']['password'] == $this->current_password) {
                if (empty($this->password) && empty($this->re_password)) {
                    $_SESSION['CP_value'] = $this->current_password;
                    $_SESSION['Prop'] = "Please provide Password";
                    header("location:account-update.php?id=$this->id");
                } else {
                    try {
                        $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET 
                        password=:password,modified=:modified WHERE unique_id=" . "'" . $this->id . "'";
                        $stmt = $this->con->prepare($query);
                        $stmt->execute(array(
                            ":password" => $this->password,
                            ":modified" => date("Y-m-d h:i:s"),
                        ));
                        $_SESSION['Acc_U'] = "Account Updated";
                        header("location:account-update.php?id=$this->id");
                    } catch (PDOException $e) {
                        echo 'Error: ' . $e->getMessage();
                    }
                }
            } else {
                $_SESSION['currentP'] = "Password dose not matched";
                header("location:account-update.php?id=$this->id");
            }
        } else {
            if ($_SESSION['Login_data']['is_admin'] == 1) {
                try {
                    $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET email=:email,is_active=:is_active,is_manager=:is_manager WHERE unique_id=" . "'" . $this->id . "'";
                    $stmt = $this->con->prepare($query);
                    $stmt->execute(array(
                        ":email" => $this->email,
                        ':is_active' => $this->is_active,
                        ':is_manager' => $this->is_manager,
                    ));
                    $_SESSION['Acc_U'] = "Account Updated";
                    header("location:account-update.php?id=$this->id");
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            } else {
                try {
                    $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET email=:email WHERE unique_id=" . "'" . $this->id . "'";
                    $stmt = $this->con->prepare($query);
                    $stmt->execute(array(
                        ":email" => $this->email,
                    ));
                    $_SESSION['Acc_U'] = "Account Updated";
                    header("location:account-update.php?id=$this->id");
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        }

    }

    public function trashItem()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE is_delete = 1";
            $query = $this->con->prepare($qur);
            $query->execute();

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        try {
            $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET is_delete=:is_delete,deleted=:deleted WHERE unique_id=" . "'" . $this->id . "'";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ":is_delete" => 1,
                ":deleted" => date("Y-m-d h:i:s"),
            ));
            $_SESSION['Sus_Acc'] = "Successfully Suspended";
            header("location:index.php?id=" . $_SESSION['Login_data']['unique_id']);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function reStore()
    {
        try {
            $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET is_delete=:is_delete WHERE unique_id=" . "'" . $this->id . "'";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ":is_delete" => 0,
            ));
            $_SESSION['res_Acc'] = "Successfully Restored";
            header("location:trash-items.php?id=" . $_SESSION['Login_data']['unique_id']);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE signup, profiles FROM signup INNER JOIN profiles ON signup.id = profiles.users_id WHERE signup.unique_id=" . "'" . $this->id . "'";
            $stmt = $this->con->prepare($query);
            $stmt->execute();
            $_SESSION['dele_Acc'] = "Successfully Deleted";
            header("location:trash-items.php?id=" . $_SESSION['Login_data']['unique_id']);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function forgot()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE email=" . "'" . $this->email . "'";
            $query = $this->con->prepare($qur);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if (!empty($row)) {
                return $row;
            } else {
                $_SESSION['Not'] = "User not found";
                header("location:forgot.php?id=$this->id");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function forgotVerify()
    {
        try {
            $qr = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE verification_id=" . "'" . $this->vid . "'";
            $query = $this->con->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['forgotU'] = $row;
            if (isset($row) && !empty($row)) {
                header("location:forgot.php?id=pconfirm");
            } else {
                $_SESSION['Not'] = "User not found";
                header("location:forgot.php?id=reset");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function forgotPasswordQuery()
    {
        try {
            $qur = "SELECT * FROM signup LEFT JOIN profiles ON signup.id = profiles.users_id WHERE user_name=" . "'" . $this->user_name . "'";
            $query = $this->con->prepare($qur);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['forgotP'] = $row;
            if ($row['user_name'] == $_SESSION['forgotU']['user_name']) {
                header("location:forgot.php?id=pcomplete");
            } else {
                $_SESSION['Not'] = "User name not found";
                header("location:forgot.php?id=pconfirm");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function forgotPasswordUpdate()
    {
        try {
            $query = "UPDATE signup JOIN profiles ON signup.id = profiles.users_id SET password=:password WHERE
unique_id=" . "'" . $_SESSION['forgotP']['unique_id'] . "'";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ":password" => $this->password,
            ));
            $_SESSION['Pass_U'] = "Password Successfully Updated. Now you could Login";
            header("location:login.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}